import { getConnection } from "./conexion.js";

// WRITE
let alumnos_db = {}
alumnos_db.insertar = function insertar(alumno) {
    return new Promise((resolve, reject) => {
        // Consulta
        let sql_consulta = "INSERT INTO alumnos SET ?";
        getConnection().query(sql_consulta, alumno, function(err, res) {
            if (err) {
                console.log(`Surgió un error: ${err.message}`);
                reject(err);
            }
            else {
                const alumno = {
                    id: res.id,
                }
                resolve(alumno);
            }
        });
    });
}

// READ
alumnos_db.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject) => {
        // Consulta
        let sql_consulta = "SELECT * FROM alumnos";
        getConnection().query(sql_consulta, null, function(err, res) {
            if (err) {
                console.log(`Surgió un error: ${err.message}`);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

export default alumnos_db;
