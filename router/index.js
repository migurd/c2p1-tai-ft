import express from 'express';
import json from 'body-parser';
import alumnos_db from '../models/alumnos.js';

export const router = express.Router();
export default {router};

// Se declara primera ruta por omisión
router.get('/', (req, res) => {
    res.render('index', {titulo: "Mis prácticas JS", nombre: "Ángel Qui"});
});

router.get('/p1', (req, res) => {
    res.render('p1/index', {titulo: "Mis prácticas JS", nombre: "Ángel Qui"});
});

router.get('/p1/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.query.numero,
    }
    res.render('p1/table', params);
});

router.post('/p1/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.body.numero,
    }
    res.render('p1/table', params);
});

router.get('/p1/cotization', (req, res) => {
    // Parámetros
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos,
    }
    res.render('p1/cotization', params);
});

router.post('/p1/cotization', (req, res) => {
    // Parámetros
    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: parseInt(req.body.plazos, 10),
    }
    res.render('p1/cotization', params);
});

router.get('/p1/alumnos', async (req, res) => {
    const rows = await alumnos_db.mostrarTodos();
    res.render('p1/alumnos', {
        reg: rows,
    });
});

router.post('/p1/alumnos', async (req, res) => {
    // Parámetros
    let params;
    try {
        params = {
            // id: req.body.id,
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad,
        }
        const res = await alumnos_db.insertar(params);
    }
    catch(err) {
        console.error(err);
        res.status(400).send(`Sucedió un error: ${err}`)
    }
    const rows = await alumnos_db.mostrarTodos();
    res.render('p1/alumnos', {
        reg: rows,
    });   
});

// **********
// PRE EXAMEN

// Acerca de 
router.get('/preexamen1', (req, res) => {
    res.render('preexamen1/index');
});

// Pago de recibo
router.get('/preexamen1/pago', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.query.numeroRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipoServicio: req.query.tipoServicio,
        kw: req.query.kw,
    }
    res.render('preexamen1/pago', params);
});

router.post('/preexamen1/pago', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.body.numeroRecibo,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipoServicio: req.body.tipoServicio,
        kw: req.body.kw,
    }
    res.render('preexamen1/pago', params);
});

// Examen c2
router.get('/examenc2', (req, res) => {
    res.render('examenc2/index');
});

// Detalles pago examen c2
router.get('/examenc2/pago', (req, res) => {
     // Parámetros
    const params = {
        num_docente: req.query.num_docente,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivel: req.query.nivel,
        pago_base: req.query.pago_base,
        horas_impartidas: req.query.horas_impartidas,
        cantidad_hijos: req.query.cantidad_hijos,
    }
   res.render('examenc2/pago', params);
});

// Detalles pago exa c2
router.post('/examenc2/pago', (req, res) => {
     // Parámetros
    const params = {
        num_docente: parseInt(req.body.num_docente),
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivel: parseInt(req.body.nivel),
        pago_base: parseFloat(req.body.pago_base),
        horas_impartidas: parseInt(req.body.horas_impartidas),
        cantidad_hijos: parseInt(req.body.cantidad_hijos),
    }
   res.render('examenc2/pago', params);
});
